const PrerenderSpaPlugin = require("prerender-spa-plugin");

process.env.VUE_APP_VERSION = process.env.VERSION
  ? process.env.VERSION
  : "0.1.0";

module.exports = {
  chainWebpack: config => {
    const isProd = process.env.NODE_ENV === "production";
    const outDir = config.output.get("path");

    if (isProd) {
      // Build hidden source maps
      config.devtool("hidden-source-map");

      // Prerender Plugin
      config.plugin("prerender").use(
        new PrerenderSpaPlugin({
          staticDir: outDir,
          routes: ["/"]
        })
      );

      // Generate manifests
      const version = process.env.VUE_APP_VERSION;
      config.plugin("copy").tap(args => [
        [
          {
            from: "./src/assets/manifest.*",
            to: ".",
            flatten: true,
            transform(content) {
              const manifest = JSON.parse(content.toString());

              manifest.version = version;

              return JSON.stringify(manifest, null, 2);
            }
          },
          ...args[0]
        ]
      ]);
    }

    // SVG path
    config.module
      .rule("svg")
      .use("file-loader")
      .loader("file-loader")
      .tap(opts => {
        opts.name = "[name].[hash:8].[ext]";
        return opts;
      });

    // Images path
    config.module
      .rule("images")
      .use("url-loader")
      .loader("url-loader")
      .tap(opts => {
        opts.fallback.options.name = "[name].[hash:8].[ext]";
        return opts;
      });

    return config;
  }
};
