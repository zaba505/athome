[![codecov](https://codecov.io/gl/zaba505/athome/branch/master/graph/badge.svg?token=x8WPsCFigw)](https://codecov.io/gl/zaba505/athome)

# athome
athome is a cross browser extension for a custom New Tab/Home page.

## Browser Support
athome is available for either [Chrome]() or [Firefox]().

## Getting Started

### WIP
- [x] Dashboard
    * [x] Add Widget
    * [x] Remove Widget
    * [x] Save Widget
    * [x] Update Widget
        - [x] Widget Settings
    * [x] Drag n' drop
        - [x] save x, y
        - [x] smooth out
    * [x] Resizable
        - [x] save width, height
        - [x] smooth out
    * [x] Update pos after L/T resize
- [x] Init git
- [x] Configure CI
- [x] Theming
- [x] Configure CD
- [x] Widgets
    * [ ] Ad
    * [x] Background
    * [x] Clock
    * [x] Calendar
    * [ ] Weather
    * [x] Search
    * [x] Greeting
    * [x] Quick Links
