describe("Resizable", () => {
  after(done => {
    const req = window.indexedDB.open("athome");
    req.onsuccess = e => {
      const db = e.target.result;

      const tx = db.transaction("widgets", "readwrite").objectStore("widgets");
      const creq = tx.clear();
      creq.onsuccess = () => done();
    };
  });

  before(() => {
    cy.visit("/");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openStoreBtn"]').click();

    cy.get('[data-text="addGreetingBtn"]').click();

    cy.get('[data-text="widgetOutline"]').click();

    cy.get('[data-text="closeStoreBtn"]').click();

    cy.get('[data-text="openMenuBtn"]').click();
  });

  afterEach(() => cy.get('[data-text="openMenuBtn"]').click());

  it("should grow width", () => {
    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="editDashboardBtn"]').click();

    cy.get('[data-text="bottom_right"]')
      .trigger("mousedown")
      .trigger("mousemove", { clientX: 100 })
      .trigger("mouseup");

    cy.get('[data-text="GreetingWidget"]').then($widget => {
      const width = $widget.css("width");
      const w = width.slice(0, width.length - 2);

      cy.wrap(w).snapshot();
    });
  });

  it("should grow height", () => {
    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="editDashboardBtn"]').click();

    cy.get('[data-text="bottom_right"]')
      .trigger("mousedown")
      .trigger("mousemove", { clientY: 100 })
      .trigger("mouseup");

    cy.get('[data-text="GreetingWidget"]').then($widget => {
      const height = $widget.css("height");
      const h = height.slice(0, height.length - 2);

      cy.wrap(h).snapshot();
    });
  });
});
