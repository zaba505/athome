describe("Draggable", () => {
  after(done => {
    const req = window.indexedDB.open("athome");
    req.onsuccess = e => {
      const db = e.target.result;

      const tx = db.transaction("widgets", "readwrite").objectStore("widgets");
      const creq = tx.clear();
      creq.onsuccess = () => done();
    };
  });

  before(() => {
    cy.visit("/");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openStoreBtn"]').click();

    cy.get('[data-text="addGreetingBtn"]').click();

    cy.get('[data-text="widgetOutline"]').click();

    cy.get('[data-text="closeStoreBtn"]').click();

    cy.get('[data-text="openMenuBtn"]').click();
  });

  it("should drag on mouse down", () => {
    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="editDashboardBtn"]').click();

    cy.get('[data-text="GreetingWidget"]')
      .trigger("mousedown")
      .trigger("mousemove", { clientY: 200 })
      .trigger("mouseup");

    cy.get('[data-text="GreetingWidget"]').then($widget => {
      const top = $widget.css("top");
      const y = top.slice(0, top.length - 2);

      cy.wrap(y).snapshot();
    });
  });
});
