describe("Feedback", () => {
  beforeEach(() => cy.visit("/"));

  after(done => {
    const req = window.indexedDB.open("athome");
    req.onsuccess = e => {
      const db = e.target.result;

      const tx = db.transaction("widgets", "readwrite").objectStore("widgets");
      const creq = tx.clear();
      creq.onsuccess = () => done();
    };
  });

  it("should be able to submit feedback", () => {
    cy.get('[data-text="openMenuBtn"]').click();
    cy.get('[data-text="openAboutBtn"]').click();

    cy.get('[data-text="openFeedbackBtn"]').click();

    cy.get('[data-text="titleField"]').type("Hello");
    cy.get('[data-text="descrField"]').type("Test bug message");

    cy.get('[data-text="nextBtn"]').click();

    cy.get('[data-text="submitFeedbackBtn"]').click();

    cy.contains("Thank you for your feedback!");
  });
});
