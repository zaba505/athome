describe("Remove Widget", () => {
  after(done => {
    const req = window.indexedDB.open("athome");
    req.onsuccess = e => {
      const db = e.target.result;

      const tx = db.transaction("widgets", "readwrite").objectStore("widgets");
      const creq = tx.clear();
      creq.onsuccess = () => done();
    };
  });

  it("should be able to remove widgets", () => {
    cy.visit("/");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openStoreBtn"]').click();

    cy.get('[data-text="addGreetingBtn"]').click();

    cy.get('[data-text="widgetOutline"]').click();

    cy.contains("Welcome to @Home");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openSettingsBtn"]').click();

    cy.get('[data-text="openGreetingSettings"]').click();

    cy.get('[data-text="removeWidgetBtn"]').click();

    cy.get('[data-text="closeSettingsBtn"]').click();

    cy.reload();

    cy.contains("Welcome to @Home").should("not.exist");
  });
});
