describe("Save Widget", () => {
  after(done => {
    const req = window.indexedDB.open("athome");
    req.onsuccess = e => {
      const db = e.target.result;

      const tx = db.transaction("widgets", "readwrite").objectStore("widgets");
      const creq = tx.clear();
      creq.onsuccess = () => done();
    };
  });

  it("should save to db", () => {
    cy.visit("/");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openStoreBtn"]').click();

    cy.get('[data-text="addGreetingBtn"]').click();

    cy.get('[data-text="widgetOutline"]').click();

    cy.reload();

    cy.contains("Welcome to @Home");
  });
});
