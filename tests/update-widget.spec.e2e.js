describe("Update Widget", () => {
  after(done => {
    const req = window.indexedDB.open("athome");
    req.onsuccess = e => {
      const db = e.target.result;

      const tx = db.transaction("widgets", "readwrite").objectStore("widgets");
      const creq = tx.clear();
      creq.onsuccess = () => done();
    };
  });

  beforeEach(() => {
    cy.visit("/");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openStoreBtn"]').click();

    cy.get('[data-text="addGreetingBtn"]').click();

    cy.get('[data-text="widgetOutline"]').click();

    cy.get('[data-text="closeStoreBtn"]').click();

    cy.get('[data-text="openMenuBtn"]').click();
  });

  afterEach(() => {
    cy.visit("/");

    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openSettingsBtn"]').click();

    cy.get('[data-text="openGreetingSettings"]').click();

    cy.get('[data-text="removeWidgetBtn"]').click();

    cy.get('[data-text="closeSettingsBtn"]').click();

    cy.get('[data-text="openMenuBtn"]').click();
  });

  it("should update db on widget state mutations", () => {
    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="openSettingsBtn"]').click();

    cy.get('[data-text="openGreetingSettings"]').click();

    cy.get('[data-text="editGreeting"]')
      .clear()
      .type("Updated Greeting{enter}");

    cy.get('[data-text="closeSettingsBtn"]').click();

    cy.reload();

    cy.contains("Updated Greeting");
  });

  it("should update db on position mutations", () => {
    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="editDashboardBtn"]').click();

    cy.get('[data-text="GreetingWidget"]')
      .trigger("mousedown", { which: 1 })
      .trigger("mousemove", { clientY: 200 })
      .trigger("mouseup", { force: true });

    cy.reload();

    cy.get('[data-text="GreetingWidget"]').then($widget => {
      const top = $widget.css("top");
      const y = top.slice(0, top.length - 2);

      cy.wrap(y).snapshot();
    });
  });

  it("should update db on size mutations", () => {
    cy.get('[data-text="openMenuBtn"]').click();

    cy.get('[data-text="editDashboardBtn"]').click();

    cy.get('[data-text="bottom_right"]')
      .trigger("mousedown", { which: 1 })
      .trigger("mousemove", { clientX: 20 })
      .trigger("mouseup", { force: true });

    cy.reload();

    cy.get('[data-text="GreetingWidget"]').then($widget => {
      const width = $widget.css("width");
      const w = width.slice(0, width.length - 2);

      cy.wrap(w).snapshot();
    });
  });
});
