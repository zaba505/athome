# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.8.2](https://gitlab.com/zaba505/athome/compare/v0.8.1...v0.8.2) (2019-12-06)


### Bug Fixes

* **fix**: vuex namespace conflict ([8264d1f](https://gitlab.com/zaba505/athome/commits/8264d1f4f589a2224c77b4e2ef00cba991c36742))

### [0.8.1](https://gitlab.com/zaba505/athome/compare/v0.8.0...v0.8.1) (2019-12-05)


### Bug Fixes

* **fix**: type reqs became more strict after deps update ([ceab535](https://gitlab.com/zaba505/athome/commits/ceab5355196483afa6d4e15c3dd9494d6fe1a6a8))

## [0.8.0](https://gitlab.com/zaba505/athome/compare/v0.7.7...v0.8.0) (2019-12-04)


### Features

* **Clock**: add tests for hours conversion ([f9817d4](https://gitlab.com/zaba505/athome/commits/f9817d4b1aba901835039c7b2892efd7cc9201c8))

### Bug Fixes

* **fix**: digital display now enhanced ([#17](https://gitlab.com/zaba505/athome/issues/17)) ([c91e490](https://gitlab.com/zaba505/athome/commits/c91e490c74ed54a10b11f41767c5698b9ab1b6a5))
* **fix**: links color now provides better contrast for both light and dark ([#16](https://gitlab.com/zaba505/athome/issues/16)) ([55c3035](https://gitlab.com/zaba505/athome/commits/55c303587e7a862e219447a53d694703053285d4))
* **fix**: digital displays now handles 12 hrs ([#57](https://gitlab.com/zaba505/athome/issues/57)) ([8409e20](https://gitlab.com/zaba505/athome/commits/8409e20a258b56afa8c49ae045b075804aa7444e))
* **fix**: v-model doesn't work in MatCheckbox ([#59](https://gitlab.com/zaba505/athome/issues/59)) ([b2097d6](https://gitlab.com/zaba505/athome/commits/b2097d680cffc26e1060434b5df25b470db81853))

### [0.7.7](https://gitlab.com/zaba505/athome/compare/v0.7.6...v0.7.7) (2019-11-28)


### Features

* fixed width/height for about dialog ([7096222](https://gitlab.com/zaba505/athome/commits/70962226ea464fc4e5203f6a61494c3893097d3f))
* impld MatTextarea ([588644f](https://gitlab.com/zaba505/athome/commits/588644f50727df64bf90427d83b34754994fd938))
* Improve feedback submittin ux ([adcbac0](https://gitlab.com/zaba505/athome/commits/adcbac05d5847c172881ab5cf09e9b09d494932e))
* Add bug reporter ([c01c572](https://gitlab.com/zaba505/athome/commits/c01c572250cbf618b6b2361e0d906226f7d3ccad))
* stories now updated on tags ([#49](https://gitlab.com/zaba505/athome/issues/49)) ([393e3a6](https://gitlab.com/zaba505/athome/commits/393e3a6c99ec0e649a67e62d2b29a7846ca4f5da))

### Bug Fixes

* **fix**: was keyscanning for github instead of gitlab ([12498da](https://gitlab.com/zaba505/athome/commits/12498daf138e5f2c24d187697b8a062548c83ade))
* **fix**: storybook deploymeny ([5a01ae2](https://gitlab.com/zaba505/athome/commits/5a01ae2c23d4aac98ecb62aa7dbb22e107828c90))
* **fix**: storybook deployment ([e6a6133](https://gitlab.com/zaba505/athome/commits/e6a613384cebd403f267ca40c2ee2ef43b105e6f))

### [0.7.6](https://gitlab.com/zaba505/athome/compare/v0.7.5...v0.7.6) (2019-11-20)


### Bug Fixes

* **fix**: alpine/git image doesn't work ([b141f34](https://gitlab.com/zaba505/athome/commits/b141f3446a45801097d29b97b46503a300798392))

### [0.7.5](https://gitlab.com/zaba505/athome/compare/v0.7.4...v0.7.5) (2019-11-20)


### Features

* cache hydration is now n+1 vs 2n ([#56](https://gitlab.com/zaba505/athome/issues/56)) ([f2ba84b](https://gitlab.com/zaba505/athome/commits/f2ba84b599c1e1718977432414d41e9bb1a27218))
* storybook is now updated on tags ([#49](https://gitlab.com/zaba505/athome/issues/49)) ([997aca1](https://gitlab.com/zaba505/athome/commits/997aca178e964abc0acd119a653f1b798a386165))

### [0.7.4](https://gitlab.com/zaba505/athome/compare/v0.7.3...v0.7.4) (2019-11-16)


### Features

* MatDialog now can be closed by outside click ([#55](https://gitlab.com/zaba505/athome/issues/55)) ([6295a77](https://gitlab.com/zaba505/athome/commits/6295a77c975895e01f2195eb3693d1592b5c22f3))
* improved deployment ([2165216](https://gitlab.com/zaba505/athome/commits/21652168ee3a7199da382868ad5214fc40ad925d))

### [0.7.3](https://gitlab.com/zaba505/athome/compare/v0.7.2...v0.7.3) (2019-11-07)


### Features

* background images cache implemented ([#52](https://gitlab.com/zaba505/athome/issues/52)) ([1ffc413](https://gitlab.com/zaba505/athome/commits/1ffc4139359e485020435af738e6e0b19c1f223d))
* fixed background provider switching ([#46](https://gitlab.com/zaba505/athome/issues/46)) ([3f1b494](https://gitlab.com/zaba505/athome/commits/3f1b494d77fecc116939e5c2881466565cd03df1))
* can now change rows/cols ([#50](https://gitlab.com/zaba505/athome/issues/50)) ([566d26e](https://gitlab.com/zaba505/athome/commits/566d26e2b0e5ff44f78c4a67ffceb2d4ed286215))

### Bug Fixes

* **fix(clock)**: improved analog hands/ticks overlap ([a3456ee](https://gitlab.com/zaba505/athome/commits/a3456eeb045df35963a737d4550e3b11d5b93c63))

### [0.7.2](https://gitlab.com/zaba505/athome/compare/v0.7.1...v0.7.2) (2019-11-03)


### Features

* storybook deploymeny added ([#49](https://gitlab.com/zaba505/athome/issues/49)) ([3f8dcb9](https://gitlab.com/zaba505/athome/commits/3f8dcb90d9ec2244f26b14b62277b58f733749dc))
* **background**: added story ([a02c04b](https://gitlab.com/zaba505/athome/commits/a02c04b39188176b9e82f96e6adfd86665e6dd18))
* added storybook deployment ([2ac8ca9](https://gitlab.com/zaba505/athome/commits/2ac8ca93d342f7d0038177ba133f9eae10e7215b))
* storybook static build now works ([33f314d](https://gitlab.com/zaba505/athome/commits/33f314d87426dc4f7e32c609e796336ea77221a1))
* **searchbar**: added story ([e122a6b](https://gitlab.com/zaba505/athome/commits/e122a6b798d7c3ab64d70924b7e40d51706abc9f))
* **clock**: added story ([b6a7000](https://gitlab.com/zaba505/athome/commits/b6a7000282b33164f9388b65de4e11bb3a7b95c6))
* **quicklinks**: added stories ([3150ca6](https://gitlab.com/zaba505/athome/commits/3150ca6b29e43dbc322baacb596602d588b0b7a2))
* using composition api for stories ([f3df2df](https://gitlab.com/zaba505/athome/commits/f3df2dfe71036d2b411bb139fa035fe0e7ddd66d))
* **greeting**: added knob for greeting message ([8523b2f](https://gitlab.com/zaba505/athome/commits/8523b2f160d18651a2ee7a9904798a7ff7b49381))
* **greeting**: created stories ([3ac3745](https://gitlab.com/zaba505/athome/commits/3ac37450910600221d630fadb044db4b591287ad))
* added storybook ([1b2b23c](https://gitlab.com/zaba505/athome/commits/1b2b23c79ad6aba8c45603596f36475e0e9ef0cc))

### Bug Fixes

* **fix(greeting)**: message knob label fixed ([3ba0428](https://gitlab.com/zaba505/athome/commits/3ba0428aa4daed5ba0fe1a90925e1666300b3d93))

### [0.7.1](https://gitlab.com/zaba505/athome/compare/v0.7.0...v0.7.1) (2019-10-29)


### Bug Fixes

* **fix**: .gitlab-ci.yml ([b040b76](https://gitlab.com/zaba505/athome/commits/b040b76addae2cdcdd3268ddc1315dfdd71619ec))

## [0.7.0](https://gitlab.com/zaba505/athome/compare/v0.6.10...v0.7.0) (2019-10-29)


### Features

* cd to chrome complete! ([#3](https://gitlab.com/zaba505/athome/issues/3)) ([ba51764](https://gitlab.com/zaba505/athome/commits/ba517648c0a02089a7c5d508c6022cbd9e84c2dd))

### [0.6.10](https://gitlab.com/zaba505/athome/compare/v0.6.9...v0.6.10) (2019-10-29)


### Bug Fixes

* **fix**: .gitlab-ci.yml ([415d308](https://gitlab.com/zaba505/athome/commits/415d308d19a91b648c6fa8cedeb860ec6fc18528))

### [0.6.9](https://gitlab.com/zaba505/athome/compare/v0.6.8...v0.6.9) (2019-10-29)


### Bug Fixes

* **fix**: .gitlab-ci.yml ([91a5641](https://gitlab.com/zaba505/athome/commits/91a56414112a216c938277a1eae87806df36bae1))

### [0.6.8](https://gitlab.com/zaba505/athome/compare/v0.6.7...v0.6.8) (2019-10-29)


### Bug Fixes

* **fix**: improve curl resp testing ([#3](https://gitlab.com/zaba505/athome/issues/3)) ([4953813](https://gitlab.com/zaba505/athome/commits/49538134744da4af808ec834d39367406a42f4f5))

### [0.6.7](https://gitlab.com/zaba505/athome/compare/v0.6.6...v0.6.7) (2019-10-29)


### Bug Fixes

* **fix**: test token curl req ([#3](https://gitlab.com/zaba505/athome/issues/3)) ([f9c1192](https://gitlab.com/zaba505/athome/commits/f9c11923568f56224c50d0277707d293d1153c56))

### [0.6.6](https://gitlab.com/zaba505/athome/compare/v0.6.5...v0.6.6) (2019-10-29)


### Bug Fixes

* **fix**: .gitlab-ci.yml ([#3](https://gitlab.com/zaba505/athome/issues/3)) ([aba3ae7](https://gitlab.com/zaba505/athome/commits/aba3ae7bcc56b10e436355351645fb87c706886b))

### [0.6.5](https://gitlab.com/zaba505/athome/compare/v0.6.4...v0.6.5) (2019-10-28)


### Bug Fixes

* **fix**: .gitlab-ci.yml ([#3](https://gitlab.com/zaba505/athome/issues/3)) ([ef9b066](https://gitlab.com/zaba505/athome/commits/ef9b06647e9b1c1214ad6cdaf4deaea4dcde6c30))

### [0.6.4](https://gitlab.com/zaba505/athome/compare/v0.6.3...v0.6.4) (2019-10-28)


### Bug Fixes

* **fix**: .gitlab-ci.yml ([96ebc14](https://gitlab.com/zaba505/athome/commits/96ebc1405bb7daf764820df2e9a1fe6fc0abc67c))

### [0.6.3](https://gitlab.com/zaba505/athome/compare/v0.6.2...v0.6.3) (2019-10-28)


### Features

* cd to chrome implemented ([#3](https://gitlab.com/zaba505/athome/issues/3)) ([45f7b7f](https://gitlab.com/zaba505/athome/commits/45f7b7fc07b2ce8fbdd61d85d9c97df7a5d96444))

### Bug Fixes

* **fix**: only case which remains is xx:59:59 -> 00:00:00 ([#47](https://gitlab.com/zaba505/athome/issues/47)) ([1990784](https://gitlab.com/zaba505/athome/commits/1990784ebf8eaa540307086c10763728b663c2b3))

### [0.6.2](https://gitlab.com/zaba505/athome/compare/v0.6.1...v0.6.2) (2019-10-17)


### Features

* **Chrome**: roughed out chrome deploy ([5632712](https://gitlab.com/zaba505/athome/commits/56327120890638f0fefd3a066de0116d8040f7fe))

### Bug Fixes

* **fix**: universalized outline border width ([#40](https://gitlab.com/zaba505/athome/issues/40)) ([f9321e2](https://gitlab.com/zaba505/athome/commits/f9321e2835adf023af8a737ad5ed310391610c40))
* **fix**: MatCheckbox now pads label correctly in Chrome ([#31](https://gitlab.com/zaba505/athome/issues/31)) ([71bef30](https://gitlab.com/zaba505/athome/commits/71bef30bcccd9801ef29350184e7f78ce70a86bd))
* **fix**: MatRadio nows pads label correctly in Chrome ([#43](https://gitlab.com/zaba505/athome/issues/43)) ([55cb6c8](https://gitlab.com/zaba505/athome/commits/55cb6c857196f83c2ae11f6ad51e78a251aa783c))
* **fix**: MatSlider now looks right in WebKit/Blink ([#44](https://gitlab.com/zaba505/athome/issues/44)) ([43cce80](https://gitlab.com/zaba505/athome/commits/43cce80cce9271be4abb1fd637a4886791aac9a3))

### [0.6.1](https://gitlab.com/zaba505/athome/compare/v0.6.0...v0.6.1) (2019-10-13)


## [0.6.0](https://gitlab.com/zaba505/athome/compare/v0.5.0...v0.6.0) (2019-10-13)


### Features

* **materialize**: unscoped material styles and fixed widget styles ([9111a67](https://gitlab.com/zaba505/athome/commits/9111a67232165674083d7538d37570363543845d))
* **MatInput**: converted to functional component ([0e4be36](https://gitlab.com/zaba505/athome/commits/0e4be36ea97d9c2ede983a94db1da76c590da8f9))
* **materialized**: removed material-components-web dep ([1ba8595](https://gitlab.com/zaba505/athome/commits/1ba85954c72281bf393af0f60e74d71446010c79))
* **materialized**: removed material-components-web comps and styles ([ed42f75](https://gitlab.com/zaba505/athome/commits/ed42f75055c16e860b6630e15bb873ce6b78ecf9))

### Bug Fixes

* **fix**: background credits are now non-selectable ([#37](https://gitlab.com/zaba505/athome/issues/37)) ([43b2c83](https://gitlab.com/zaba505/athome/commits/43b2c83028cb1a906cda9e9cb18833537ff92c9b))
* **fix**: normalized system time and fixed zone offset ([#18](https://gitlab.com/zaba505/athome/issues/18)) ([9de4d90](https://gitlab.com/zaba505/athome/commits/9de4d90831f43d85f285044b6c611e9203d0a095))
* **fix**: MatRadio now checks on init with v-model value ([#34](https://gitlab.com/zaba505/athome/issues/34)) ([5eb7aac](https://gitlab.com/zaba505/athome/commits/5eb7aac7fe9fd0ccc39fc4658a352b2180964e2b))
* **fix**: MatCheckbox margin now removed ([#33](https://gitlab.com/zaba505/athome/issues/33)) ([561a553](https://gitlab.com/zaba505/athome/commits/561a553577367cbac14c62d7e6710e6b647db218))
* **fix(material)**: missed a @material import ([96e8b52](https://gitlab.com/zaba505/athome/commits/96e8b5236061ae2aef503122bc08a4ca482d7944))
* **fix**: implemented MatList and MatIconButton changes ([067ec22](https://gitlab.com/zaba505/athome/commits/067ec221c0af6ef6903d110b1cb877e7ec7ec166))

## [0.5.0](https://gitlab.com/zaba505/athome/compare/v0.4.2...v0.5.0) (2019-12-05)


### [0.4.2](https://gitlab.com/zaba505/athome/compare/v0.4.1...v0.4.2) (2019-12-05)


### [0.4.1](https://gitlab.com/zaba505/athome/compare/v0.4.0...v0.4.1) (2019-12-05)


## [0.4.0](https://gitlab.com/zaba505/athome/compare/v0.3.7...v0.3.0) (2019-12-05)


### [0.3.7](https://gitlab.com/zaba505/athome/compare/v0.3.6...v0.3.7) (2019-12-05)


### [0.3.6](https://gitlab.com/zaba505/athome/compare/v0.3.5...v0.3.6) (2019-12-05)


### [0.3.5](https://gitlab.com/zaba505/athome/compare/v0.3.4...v0.3.5) (2019-12-05)


### [0.3.4](https://gitlab.com/zaba505/athome/compare/v0.3.3...v0.3.4) (2019-12-05)


### [0.3.3](https://gitlab.com/zaba505/athome/compare/v0.3.2...v0.3.3) (2019-12-05)


### [0.3.2](https://gitlab.com/zaba505/athome/compare/v0.3.1...v0.3.2) (2019-12-05)


### [0.3.1](https://gitlab.com/zaba505/athome/compare/v0.3.0...v0.3.1) (2019-12-05)


## [0.3.0](https://gitlab.com/zaba505/athome/compare/v0.2.2...v0.3.1) (2019-12-05)


### [0.2.2](https://gitlab.com/zaba505/athome/compare/v0.2.1...v0.2.2) (2019-12-05)


### [0.2.1](https://gitlab.com/zaba505/athome/compare/v0.2.0...v0.2.1) (2019-12-05)


## [0.2.0](https://gitlab.com/zaba505/athome/compare/v0.1.3...v0.2.0) (2019-12-05)


### [0.1.3](https://gitlab.com/zaba505/athome/compare/v0.1.2...v0.1.3) (2019-12-05)


### [0.1.2](https://gitlab.com/zaba505/athome/compare/v0.1.1...v0.1.2) (2019-12-05)


## [0.1.1](https://gitlab.com/zaba505/athome/compare/v0.0.8...v0.1.1) (2019-12-05)


### [0.0.8](https://gitlab.com/zaba505/athome/compare/v0.0.7...v0.0.8) (2019-12-05)


### [0.0.7](https://gitlab.com/zaba505/athome/compare/v0.0.6...v0.0.7) (2019-12-05)


### [0.0.6](https://gitlab.com/zaba505/athome/compare/v0.0.5...v0.0.6) (2019-12-05)


### [0.0.5](https://gitlab.com/zaba505/athome/compare/v0.0.4...v0.0.5) (2019-12-05)


### [0.0.4](https://gitlab.com/zaba505/athome/compare/v0.0.3...v0.0.4) (2019-12-05)


### [0.0.3](https://gitlab.com/zaba505/athome/compare/v0.0.2...v0.0.3) (2019-12-05)


### [0.0.2](https://gitlab.com/zaba505/athome/compare/v0.0.1...v0.0.2) (2019-12-05)


## 0.0.1 (2019-12-05)
