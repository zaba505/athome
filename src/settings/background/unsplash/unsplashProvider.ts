import { Store } from "vuex";
import { computed, ref, Ref, watch } from "@vue/composition-api";
import UnsplashModule, {
  maxCacheSize
} from "@/settings/background/unsplash/Unsplash.module";

const UNSPLASH_API_KEY = process.env.VUE_APP_UNSPLASH_API_KEY;
const UNSPLASH_UTM =
  "?utm_source=Start&utm_medium=referral&utm_campaign=api-credit";

interface Image {
  data: Blob;
  image_link: string;
  location_title?: string;
  user_name: string;
  user_link: string;
}

type CommitFunc = (type: string, payload?: any) => void;

export default async function(
  mod: string,
  store: Store<any>,
  color: Ref<string>,
  image: Ref<string>,
  credits: Ref<any[]>,
  location: Ref<string>
) {
  const rootState = computed(() => store.state.widgets[mod]);
  const commit = (type: string, payload?: any) =>
    store.commit(`widgets/${mod}/${type}`, payload);

  const preserve = Object.keys(rootState.value).includes("Unsplash")
    ? Array.isArray(rootState.value.Unsplash.cache)
    : false;
  store.registerModule(["widgets", mod, "Unsplash"], UnsplashModule(), {
    preserveState: preserve
  });

  const imgLink = ref("");
  const fetchUrl = computed<string>(
    () => store.getters[`widgets/${mod}/Unsplash/url`]
  );
  const indx = computed<number>({
    get: () => rootState.value.Unsplash.indx,
    set: v => commit("Unsplash/setIndx", v)
  });

  const stopCache = watch(
    () => fetchUrl.value,
    async url => await populateCache(url, commit),
    {
      lazy: true
    }
  );

  const stopIndx = watch(
    () => indx.value,
    async (v, o) => {
      if (v > 0 || !o) return;

      await populateCache(fetchUrl.value, commit);
    }
  );

  const stopHits = watch(
    () => [fetchUrl.value, rootState.value.Unsplash.cache],
    ([u, v], o) => {
      if (o) {
        if (u === o[0] && v.length === o[1].length) return;
      }
      if (v.length < 1) return;

      if (indx.value in v) hitCache(indx.value, v, imgLink, credits, location);

      indx.value = (indx.value + 1) % maxCacheSize;
    }
  );

  const stopImgLink = watch(
    () => imgLink.value,
    (v, o, onCleanup) => {
      if (!v) return;
      image.value = v;

      if (o) onCleanup(() => URL.revokeObjectURL(o));
    }
  );

  if (rootState.value.Unsplash.cache.length === 0)
    await populateCache(fetchUrl.value, commit);

  return () => {
    stopIndx();
    stopImgLink();
    stopHits();
    stopCache();

    store.unregisterModule(["widgets", mod, "Unsplash"]);
  };
}

const populateCache = async (url: string, commit: CommitFunc) => {
  const images = await fetchImages(url);

  commit("Unsplash/initCache", images);
};

const hitCache = (
  indx: number,
  cache: any[],
  imgLink: Ref<string>,
  credits: Ref<any[]>,
  location: Ref<string>
) => {
  const { res, data } = cache[indx];

  imgLink.value = URL.createObjectURL(data);

  credits.value = [
    {
      name: "Photo",
      link: res.links.html + UNSPLASH_UTM
    },
    {
      name: res.user.name,
      link: res.user.links.html + UNSPLASH_UTM
    },
    {
      name: "Unsplash",
      link: `https://unsplash.com/${UNSPLASH_UTM}`
    }
  ];

  location.value = res.location ? res.location.title : "";
};

const fetchImages = async (apiUrl: string) => {
  const headers = new Headers();
  headers.append("Authorization", `Client-ID ${UNSPLASH_API_KEY}`);

  const resp = await fetch(apiUrl, { headers });

  const results = await resp.json();

  const images = await Promise.all(
    results.map((res: any) =>
      fetch(res.urls.raw + "?q=85&w=1920")
        .then(v => v.blob())
        .then(data => ({ res, data }))
    )
  );

  return images;
};
