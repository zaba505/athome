import { Module } from "vuex";

const officialCollection = 1053828;

export const maxCacheSize = 4;

interface unsplashState {
  provider: "official" | "collections" | "search";
  refresh: number;
  collections: string;
  featured: boolean;
  search: string;
  cache: any[];
  indx: number;
}

export default function(): Module<unsplashState, any> {
  return {
    namespaced: true,
    state: {
      provider: "official",
      refresh: 0,
      collections: "",
      featured: true,
      search: "",
      cache: [],
      indx: 0
    },
    getters: {
      url: state => {
        let s = "https://api.unsplash.com/photos/random?";
        switch (state.provider) {
          case "collections":
            s += `collections=${state.collections}`;
            break;

          case "search":
            s +=
              "orientation=landscape" +
              (state.featured ? "&featured=true" : "") +
              (state.search ? `&query=${state.search}` : "");
            break;

          default:
            s += `collections=${officialCollection}`;
        }
        s += `&count=${maxCacheSize}`;

        return s;
      }
    },
    mutations: {
      changeProvider(state, p: "official" | "collections" | "search") {
        state.provider = p;
      },
      changeCollections(state, collections: string) {
        state.collections = collections;
      },
      updateFeatured(state, featured: boolean) {
        state.featured = featured;
      },
      changeSearch(state, search: string) {
        state.search = search;
      },
      initCache(state, vals: any[]) {
        state.cache = vals;
      },
      setIndx(state, n: number) {
        state.indx = n;
      }
    }
  };
}
