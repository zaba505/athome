import { Store } from "vuex";
import { computed, Ref, watch } from "@vue/composition-api";

export default async function(
  mod: string,
  store: Store<any>,
  color: Ref<string>,
  image: Ref<string>,
  credits: Ref<any[]>,
  location: Ref<string>
) {
  const rootState = computed(() => store.state.widgets[mod]);

  const preserve = Object.keys(store.state.widgets[mod]).includes("Color");
  store.registerModule(
    ["widgets", mod, "Color"],
    {
      namespaced: true,
      state: {
        color: "#fff"
      },
      mutations: {
        changeColor(state, color: string) {
          state.color = color;
        }
      }
    },
    {
      preserveState: preserve
    }
  );

  const stop = watch(
    () => rootState.value.Color.color,
    v => (color.value = v)
  );

  return () => {
    stop();
    store.unregisterModule(["widgets", mod, "Color"]);
  };
}
