import { createLocalVue, mount } from "@vue/test-utils";
import VueComposition from "@vue/composition-api";
import FeedbackForm from "@/components/about/FeedbackForm.vue";

const localVue = createLocalVue();
localVue.use(VueComposition);

describe("FeedbackForm", () => {
  it("should validate", async () => {
    const wrapper = mount(FeedbackForm, { localVue });

    await wrapper.vm.$nextTick();

    wrapper.find('[data-text="titleField"]').setValue("Test");
    wrapper.find('[data-text="descrField"]').setValue("test bug message");

    await wrapper.vm.$nextTick();

    expect(wrapper.emitted("valid")).toHaveLength(2);
  });
});
