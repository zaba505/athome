import { reactive, ref, toRefs } from "@vue/composition-api";

// useDraggable to for making a component draggable.
export function useDraggable(
  xs: number,
  ys: number,
  onEnd: (xPos: number, yPos: number) => void
) {
  const xStart = ref(0);
  const yStart = ref(0);

  const pos = reactive({
    x: xs,
    y: ys
  });

  const updateMoveState = (e: any) => {
    const dx = e.clientX - xStart.value;
    const dy = e.clientY - yStart.value;

    if (dx !== 0) pos.x += dx;
    if (dy !== 0) pos.y += dy;

    xStart.value = e.clientX;
    yStart.value = e.clientY;
  };

  const drag = (e: any) => {
    xStart.value = e.clientX;
    yStart.value = e.clientY;

    const onMoveEnd = () => {
      document.removeEventListener("mousemove", updateMoveState);
      document.removeEventListener("mouseup", onMoveEnd);

      onEnd(pos.x, pos.y);
    };

    document.addEventListener("mousemove", updateMoveState);
    document.addEventListener("mouseup", onMoveEnd);
  };

  return {
    ...toRefs(pos),
    drag
  };
}
