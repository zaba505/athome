import { reactive, ref, watch } from "@vue/composition-api";

type WatchCallback = (
  newValues: number[],
  oldValues: number[],
  onCleanup: any
) => any;

// useResizable is for making a component resizable.
export function useResizable(ws: number, hs: number) {
  const pos = reactive({
    x: 0,
    y: 0
  });

  const width = ref(ws);
  const height = ref(hs);

  const updatePos = (e: any) => {
    pos.x = e.clientX;
    pos.y = e.clientY;
  };

  const onResizeStart = (
    onWatch: WatchCallback,
    onEnd: (w: number, h: number) => void
  ) => (e: any) => {
    pos.x = e.clientX;
    pos.y = e.clientY;

    const unwatch = watch(() => [pos.x, pos.y], onWatch, { lazy: true });

    const stopResizing = (e: any) => {
      document.removeEventListener("mousemove", updatePos);
      document.removeEventListener("mouseup", stopResizing);

      onEnd(width.value, height.value);
      unwatch();
    };

    document.addEventListener("mousemove", updatePos);
    document.addEventListener("mouseup", stopResizing);
  };

  return {
    width,
    height,
    onResizeStart
  };
}
