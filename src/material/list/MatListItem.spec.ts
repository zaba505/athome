import { shallowMount } from "@vue/test-utils";
import MatListItem from "@/material/list/MatListItem.vue";

describe("MatListItem", () => {
  it("should render", () => {
    const wrapper = shallowMount(MatListItem);

    expect(wrapper.html()).toBeTruthy();
  });

  it("should render with graphic and meta", () => {
    const wrapper = shallowMount(MatListItem, {
      slots: {
        default: [
          "<span>graphic</span>",
          "<span>text</span>",
          "<span>meta</span>"
        ]
      }
    });

    expect(wrapper.findAll("span").length).toBe(3);
  });
});
