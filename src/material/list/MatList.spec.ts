import { shallowMount } from "@vue/test-utils";
import MatList from "@/material/list/MatList.vue";

describe("MatList", () => {
  it("should render", () => {
    const wrapper = shallowMount(MatList);

    expect(wrapper.html()).toBeTruthy();
  });

  it("should render all children", () => {
    const wrapper = shallowMount(MatList, {
      slots: {
        default: ["<li>1</li>", "<li>2</li>", "<li>3</li>"]
      }
    });

    expect(wrapper.findAll("li").length).toBe(3);
  });
});
