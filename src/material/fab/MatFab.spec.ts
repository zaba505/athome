import { shallowMount } from "@vue/test-utils";
import MatFab from "@/material/fab/MatFab.vue";

describe("MatFab", () => {
  it("should render", () => {
    const wrapper = shallowMount(MatFab);

    expect(wrapper.html()).toBeTruthy();
  });

  it("should render child icon", () => {
    const wrapper = shallowMount(MatFab, {
      slots: {
        default: "<i class='material-icons'>menu</i>"
      }
    });

    expect(wrapper.text()).toMatch("menu");
  });
});
