import Vue from "vue";
import App from "./App.vue";
import VueCompositionApi from "@vue/composition-api";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";

Vue.use(VueCompositionApi);

Vue.config.productionTip = false;

// Init Sentry error handling only in production
if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: process.env.VUE_APP_SENTRY_DSN,
    integrations: [
      new Integrations.Vue({ Vue, attachProps: true, logErrors: true })
    ],
    release: process.env.VUE_APP_VERSION
  });
}

new Vue({
  render: h => h(App)
}).$mount("#app");
