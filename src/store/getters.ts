import { GetterTree } from "vuex";
import { RootState } from "@/store/types";

export const getters: GetterTree<RootState, any> = {
  widgetMod: state => (name: string) =>
    !(state as any).widgets[name] ? name : `${name}-${Date.now()}`
};
