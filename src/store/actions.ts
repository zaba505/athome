import { ActionTree } from "vuex";
import { RootState } from "@/store/types";

export const actions: ActionTree<any, RootState> = {
  installWidget({ commit, state }, { factory, name, widgetState }) {
    Object.defineProperty(factory, "name", { value: name });
    Object.defineProperty(factory, "preserveState", {
      value: !!widgetState
    });

    if (widgetState) {
      this.registerModule(["widgets", name], {
        state: widgetState
      });
    }

    commit("addToDashboard", factory);
  }
};
