import Vuex, { Store } from "vuex";
import { ReplaySubject } from "rxjs";
import { createLocalVue } from "@vue/test-utils";
import { map, switchMap } from "rxjs/operators";
import { getters } from "@/store/getters";
import { mutations } from "@/store/mutations";
import { actions } from "@/store/actions";
import sync from "@/store/sync/index";

import { handleMutations, loadDashboard, watchWidgets } from "@/store/sync";

const localVue = createLocalVue();
localVue.use(Vuex);

const testDB = (
  add = jest.fn(),
  del = jest.fn(),
  getAll = jest.fn(),
  update = jest.fn()
) => ({
  add,
  delete: del,
  getAll,
  update
});

describe("sync", () => {
  let store: Store<any>;
  let callSubj: ReplaySubject<any>;

  beforeEach(() => {
    callSubj = new ReplaySubject(1);

    store = new Vuex.Store({
      state: {
        factories: [],
        editable: false
      },
      getters: getters,
      mutations: mutations,
      actions: actions,
      modules: {
        dashboard: {
          namespaced: true,
          modules: {
            test: {
              namespaced: true,
              state: {
                x: 0,
                y: 0,
                width: 0,
                height: 0
              },
              mutations: {
                updatePosition(state, { x, y }) {
                  state.x = x;
                  state.y = y;
                },
                updateDims(state, { width, height }) {
                  state.width = width;
                  state.height = height;
                }
              }
            }
          }
        },
        widgets: {
          namespaced: true,
          modules: {
            test: {
              namespaced: true,
              state: {
                description: "Test",
                count: 0
              },
              mutations: {
                add(state, n) {
                  state.count += n;
                }
              }
            }
          }
        }
      }
    });
  });

  describe("watchWidgets", () => {
    it("should react to widget module registration", done => {
      const add = jest.fn();
      add.mockImplementation(async (name: string, state: any) => {
        callSubj.next({ name: name, state: state });
        return 1;
      });

      const handlerSubj = watchWidgets(testDB(add), store, new Map(), {});

      store.registerModule(["widgets", "test2"], {
        state: {
          description: "Test"
        }
      });

      callSubj
        .pipe(
          switchMap(v => handlerSubj.pipe(map(u => ({ handlers: u, args: v }))))
        )
        .subscribe(({ args, handlers }) => {
          expect(add.mock.calls.length).toBe(1);
          expect(args.name).toBe("test2");
          expect(args.state).toEqual({ description: "Test" });

          expect(Object.keys(handlers).length).toBe(1);

          done();
        });
    });
  });

  describe("handleMutations", () => {
    it("should react to widget mutations", done => {
      const update = jest
        .fn()
        .mockImplementation(async (id: string | number, state: any) =>
          callSubj.next({ id: id, state: state })
        );

      const handlers = new ReplaySubject<any>(1);
      handlers.next({ test: (state: any) => update(1, state) });

      handleMutations(store, handlers);

      store.commit("widgets/test/add", 1);

      callSubj.subscribe(args => {
        if (args.state.count === 2) {
          done();
          return;
        }

        expect(update.mock.calls.length).toBe(1);
        expect(args.id).toBe(1);
        expect(args.state).toEqual({
          description: "Test",
          count: 1,
          x: 0,
          y: 0,
          width: 0,
          height: 0
        });

        store.commit("widgets/test/add", 1);
      });
    });
  });

  describe("loadDashboard", () => {
    it("should hydrate store from db", done => {
      const getAll = jest.fn().mockImplementation(async () => [
        {
          id: 1,
          state: {
            name: "test2",
            src: "Test",
            description: "Test",
            x: 0,
            y: 0,
            width: 100,
            height: 100
          }
        }
      ]);

      loadDashboard(
        () => Promise.resolve(testDB(undefined, undefined, getAll)),
        store
      ).then(({ widgets }) => {
        expect(widgets.has("test2")).toBe(true);
        expect(widgets.get("test2")).toBe(1);

        done();
      });
    });
  });
});
