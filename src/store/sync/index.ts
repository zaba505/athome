import { Module, MutationPayload, Store } from "vuex";
import { from, ReplaySubject, Subject } from "rxjs";
import { filter, map, mergeMap, tap, withLatestFrom } from "rxjs/operators";
import { Dims, Pos, WidgetConfig } from "@/store/types";
import * as Sentry from "@sentry/browser";

const cloneDeep = require("lodash.clonedeep");

// WidgetDB is responsible for persisting widget state
export interface WidgetDB {
  // add
  add<T extends WidgetConfig>(name: string, state: T): Promise<string | number>;

  // delete
  delete(id: string | number): Promise<void>;

  // getAll
  getAll<T extends WidgetConfig>(): Promise<
    Array<{ id: string | number; state: T }>
  >;

  // update
  update<T extends Partial<WidgetConfig>>(
    id: string | number,
    state: T
  ): Promise<void>;
}

// sync syncs a Widget state from the Vuex store to a WidgetDB
export default function(
  f: WidgetDB | (() => Promise<WidgetDB>)
): (store: Store<any>) => void {
  return store => {
    if (typeof f !== "function") f = () => Promise.resolve(f as WidgetDB);

    // Load any saved widgets from DB to dashboard
    loadDashboard(f, store)
      .then(({ db, widgets, handlers }) => {
        // Create widget state handlers upon widget module registration/unregistration
        const handlerSubj = watchWidgets(db, store, widgets, handlers);

        // Map store mutations to widget handlers
        handleMutations(store, handlerSubj);
      })
      .catch(err =>
        Sentry.withScope(scope => {
          Sentry.setTag("sync", "failed to load dashboard");

          Sentry.captureException(err);
        })
      );
  };
}

function dashboardModule(state: Pos & Dims): Module<Pos & Dims, any> {
  return {
    namespaced: true,
    state: state,
    mutations: {
      updatePosition(state, { x, y }) {
        state.x = x;
        state.y = y;
      },
      updateDims(state, { width, height }) {
        state.width = width;
        state.height = height;
      }
    }
  };
}

// loadDashboard hydrates Vuex state from the WidgetDB
export async function loadDashboard<T extends Partial<WidgetConfig>>(
  f: () => Promise<WidgetDB>,
  store: Store<any>
) {
  const db = await f();
  const widgetStates = await db.getAll<WidgetConfig & Pos & Dims>();

  const installs = widgetStates.map(({ id, state }) => {
    const name = store.getters.widgetMod(state.name);

    store.registerModule(
      ["dashboard", name],
      dashboardModule({
        x: state.x,
        y: state.y,
        width: state.width,
        height: state.height
      })
    );

    delete state.x;
    delete state.y;
    delete state.width;
    delete state.height;

    return store
      .dispatch("installWidget", {
        factory: () =>
          import(`@/widgets/${state.src.toLowerCase()}/${state.src}.vue`),
        name: name,
        widgetState: state
      })
      .then(() => ({ id: id, name: name }));
  });

  return await Promise.all(installs)
    .then(v =>
      v.reduce(
        (acc, v) => {
          acc.widgets.set(v.name, v.id);
          acc.handlers[v.name] = state => db.update(v.id, state);
          return acc;
        },
        {
          widgets: new Map<string, string | number>(),
          handlers: <Handlers<T>>{}
        }
      )
    )
    .then(v => ({ db: db, ...v }));
}

interface Handler<T extends Partial<WidgetConfig>> {
  (state: T): Promise<void>;
}

interface Handlers<T> {
  [name: string]: Handler<T>;
}

interface WidgetOp {
  op: string;
  name: string;
  state?: any;
}

// watchWidgets watches for the registering/unregistering of widgets
export function watchWidgets<T extends Partial<WidgetConfig>>(
  db: WidgetDB,
  store: Store<any>,
  widgets: Map<string, string | number>,
  handlers: Handlers<T>
): ReplaySubject<Handlers<T>> {
  // Observe widget module registering/unregistering
  const widgetSubj = new Subject<WidgetOp>();
  store.watch(
    state => Object.keys(state.widgets),
    (n, o) => {
      const deletedKeys = o.filter(e => !n.includes(e));
      const newKeys = n.filter(e => !o.includes(e));

      deletedKeys.forEach(v => widgetSubj.next({ op: "delete", name: v }));

      newKeys.forEach(v =>
        widgetSubj.next({
          op: "add",
          name: v,
          state: { ...store.state.widgets[v], ...store.state.dashboard[v] }
        })
      );
    }
  );

  const handlerSubj = new ReplaySubject<Handlers<T>>(1);
  if (Object.keys(handlers).length > 0) handlerSubj.next(handlers);

  widgetSubj
    .pipe(
      mergeMap(v =>
        v.op === "add"
          ? from(db.add(v.name, v.state)).pipe(
              tap(u => widgets.set(v.name, u)),
              tap(u => (handlers[v.name] = state => db.update(u, state)))
            )
          : from(db.delete(widgets.get(v.name) as string | number)).pipe(
              tap(() => delete handlers[v.name])
            )
      )
    )
    .subscribe({
      next: () => handlerSubj.next(handlers),
      error: err =>
        Sentry.withScope(scope => {
          Sentry.setTag("watchWidgets", "failed updating handlers");

          Sentry.captureException(err);
        })
    });

  return handlerSubj;
}

// handleMutations synchronizes widget state mutations with the WidgetDB
export function handleMutations<T extends Partial<WidgetConfig>>(
  store: Store<any>,
  handlerSubj: ReplaySubject<Handlers<T>>
) {
  const mutations = new Subject<{ mutation: MutationPayload; state: any }>();
  store.subscribe((mutation, state) => mutations.next({ mutation, state }));

  mutations
    .pipe(
      filter(({ mutation }) => mutation.type.indexOf("/") !== -1),
      map(({ mutation, state }) => {
        const name = mutation.type.split("/")[1];
        const s = { ...state.widgets[name], ...state.dashboard[name] };

        return { name: name, state: cloneDeep(s) };
      }),
      withLatestFrom(handlerSubj),
      mergeMap(([mutation, handlers]) =>
        handlers[mutation.name](mutation.state)
      )
    )
    .subscribe({
      error: err =>
        Sentry.withScope(scope => {
          Sentry.setTag("handleMutations", "failed syncing state");

          Sentry.captureException(err);
        })
    });
}
