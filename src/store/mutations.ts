import { MutationTree } from "vuex";
import { RootState } from "@/store/types";

export const mutations: MutationTree<RootState> = {
  addToDashboard(state: any, factory: () => Promise<any>) {
    state.factories.push(factory);
  },
  removeFromDashboard(state: any, name: string) {
    let found = false;
    let i;
    for (i = 0; i < state.factories.length; i++) {
      found = state.factories[i].name === name;
      if (found) {
        break;
      }
    }
    if (!found) return;

    state.factories.splice(i, 1);
  },
  editDashboard(state: any, editable: boolean) {
    state.editable = editable;
  }
};
