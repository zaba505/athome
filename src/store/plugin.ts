import { Store } from "vuex";
import { inject, provide } from "@vue/composition-api";

// StoreSymbol - currently only exported for use in vue-test-utils
export const StoreSymbol = Symbol();

// provideStore
export const provideStore = <T>(store: Store<T>) => provide(StoreSymbol, store);

// useStore
export const useStore = (): Store<any> => inject(StoreSymbol) as Store<any>;
