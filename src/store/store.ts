import Vue from "vue";
import Vuex, { Store } from "vuex";
import { getters } from "@/store/getters";
import { mutations } from "@/store/mutations";
import { actions } from "@/store/actions";
import sync from "@/store/sync";
import IndexDB from "@/store/indexdb";

Vue.use(Vuex);

const SettingsFactory = () => import("@/widgets/settings/Settings.vue");
Object.defineProperty(SettingsFactory, "name", { value: "settings" });
Object.defineProperty(SettingsFactory, "preserveState", { value: false });

export default new Vuex.Store<any>({
  state: {
    factories: [SettingsFactory],
    editable: false
  },
  getters: getters,
  mutations: mutations,
  actions: actions,
  modules: {
    widgets: {
      namespaced: true
    },
    dashboard: {
      namespaced: true,
      modules: {
        settings: {
          namespaced: true,
          state: {
            x: 0,
            y: 0,
            width: 75,
            height: 176
          }
        }
      }
    }
  },
  plugins:
    process.env.NODE_ENV === "production" ? [sync(() => IndexDB.open())] : []
});
