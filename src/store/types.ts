export interface WidgetConfig {
  name: string;
  description: string;
  src: string;
}

export type Pos = { x: number; y: number };
export type Dims = { width: number; height: number };

export interface RootState {
  factories: Array<() => Promise<any>>;
  editable: boolean;
}
