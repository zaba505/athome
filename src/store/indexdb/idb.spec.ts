import IndexDB from "@/store/indexdb";

require("fake-indexeddb/auto");

describe("Index", () => {
  let db: IndexDB;

  beforeAll(done => {
    IndexDB.open()
      .then(v => {
        db = v;
        return db.add<any>("test", {
          name: "Test",
          description: "Test",
          src: "Test"
        });
      })
      .finally(() => done());
  });

  it("should return object id", async () =>
    expect(
      await db.add<any>("test2", {
        name: "Test",
        description: "Test",
        src: "Test"
      })
    ).toBe(2));

  it("should return objects", async () => {
    const data = await db.getAll();

    expect(data.length).toBe(2);
    expect(data[0].id).toBe(1);
    expect(data[0].state.name).toBe("Test");
  });
});
