import { IDBPDatabase, openDB } from "idb";
import { WidgetDB } from "@/store/sync";
import { WidgetConfig } from "@/store/types";

const DB_NAME = "athome";
const DB_VERSION = 1;

export default class IndexDB implements WidgetDB {
  constructor(private idb: IDBPDatabase) {}

  static async open() {
    const i = await openDB(DB_NAME, DB_VERSION, {
      upgrade(db) {
        db.createObjectStore("widgets", {
          keyPath: "id",
          autoIncrement: true
        });
      }
    });

    return new IndexDB(i);
  }

  public async add<T extends WidgetConfig>(name: string, state: T) {
    const tx = this.idb.transaction("widgets", "readwrite");

    const id = await tx.objectStore("widgets").add(state);
    await tx.done;

    return id as number;
  }

  public async getAll<T extends WidgetConfig>() {
    const tx = this.idb.transaction("widgets");

    const data = await tx.objectStore("widgets").getAll();
    await tx.done;

    return data.map(v => {
      const id = v.id;
      delete v.id;

      return { id: id, state: v };
    });
  }

  public async update<T extends Partial<WidgetConfig>>(
    id: string | number,
    state: T
  ) {
    const s = Object.assign({}, state);

    const tx = this.idb.transaction("widgets", "readwrite");

    await tx.objectStore("widgets").put({ id: id, ...s });
    await tx.done;
  }

  public async delete(id: string | number) {
    const tx = this.idb.transaction("widgets", "readwrite");

    await tx.objectStore("widgets").delete(id);
    await tx.done;
  }
}
