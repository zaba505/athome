import { WidgetConfig } from "@/store/types";
import { Module } from "vuex";

interface Link {
  name: string;
  url: string;
}

interface quicklinksState {
  columns: number;
  rows: number;
  links: Link[];
}

export default function(
  name: string,
  settings: () => any
): Module<WidgetConfig & quicklinksState, any> {
  return {
    namespaced: true,
    state: {
      name: name,
      description: "Have your favorite sites just one click away.",
      src: "QuickLinks",
      columns: 4,
      rows: 2,
      links: []
    },
    getters: {
      settings: () => settings
    },
    mutations: {
      updateColumns(state, columns: number) {
        state.columns = columns;
      },
      updateRows(state, rows: number) {
        state.rows = rows;
      },
      pushLink(state, link: Link) {
        state.links.push(link);
      },
      updateLink(state, { i, link }) {
        state.links.splice(i, 1, link);
      },
      removeLink(state, name: string) {
        state.links = state.links.filter(l => l.name !== name);
      },
      changeName(state, name: string) {
        state.name = name;
      }
    }
  };
}
