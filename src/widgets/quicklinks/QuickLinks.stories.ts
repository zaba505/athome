import Vue from "vue";
import VueComposition, {
  createComponent,
  onMounted,
  watch
} from "@vue/composition-api";
import store from "@/store/story";
import { provideStore } from "@/store/plugin";
import { withKnobs, array, number } from "@storybook/addon-knobs";
import QuickLinks from "@/widgets/quicklinks/QuickLinks.vue";

Vue.use(VueComposition);

export default {
  title: "QuickLinks",
  decorators: [withKnobs]
};

export const WithKnobs = () =>
  createComponent({
    components: { QuickLinks },
    template: '<QuickLinks module="quickLinks"></QuickLinks>',
    props: {
      links: {
        default: array("Links", ["https://google.com"])
      },
      rows: {
        default: number("Rows", 2)
      },
      columns: {
        default: number("Columns", 4)
      }
    },
    setup(props, { root }) {
      provideStore(store);

      watch(
        () => props.links,
        (v, o) => {
          const n = v.filter(e => !o.includes(e));
          const old = o.filter(e => !v.includes(e));

          n.forEach(e => {
            try {
              const url = new URL(e);

              store.commit("quickLinks/pushLink", {
                name: url.hostname,
                url: e
              });
            } catch (err) {
              console.log(err);
            }
          });

          old.forEach(e => {
            try {
              const url = new URL(e);

              store.commit("quickLinks/removeLink", url.hostname);
            } catch (err) {
              console.log(err);
            }
          });
        },
        {
          lazy: true
        }
      );

      watch(
        () => props.rows,
        v => store.commit("quickLinks/updateRows", v),
        {
          lazy: true
        }
      );

      watch(
        () => props.columns,
        v => store.commit("quickLinks/updateColumns", v),
        { lazy: true }
      );

      onMounted(() =>
        props.links.forEach(e =>
          store.commit("quickLinks/pushLink", {
            name: new URL(e).hostname,
            url: e
          })
        )
      );

      return {};
    }
  });
