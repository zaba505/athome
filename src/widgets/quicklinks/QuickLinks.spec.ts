import { createLocalVue, shallowMount } from "@vue/test-utils";
import VueComposition from "@vue/composition-api";
import Vuex, { Store } from "vuex";
import { StoreSymbol } from "@/store/plugin";
import QuickLinks from "@/widgets/quicklinks/QuickLinks.vue";

const localVue = createLocalVue();

localVue.use(VueComposition);
localVue.use(Vuex);

function wrapQuickLinks(quicklinks: any, store: Store<any>) {
  return shallowMount(quicklinks, {
    propsData: {
      module: "QuickLinks"
    },
    localVue,
    provide: {
      [StoreSymbol]: store
    }
  });
}

describe("QuickLinks", () => {
  let store: Store<any>;

  beforeEach(() => {
    store = new Store({
      modules: {
        widgets: {}
      }
    });
  });

  it("should render text", async () => {
    const wrapper = wrapQuickLinks(QuickLinks, store);

    store.commit("QuickLinks/pushLink", {
      name: "@Home",
      url: "https://zaba505.gitlab.io/athome"
    });

    await wrapper.vm.$nextTick();

    expect(wrapper.text()).toMatch("@Home");
  });
});
