import Vue from "vue";
import VueComposition, {
  createComponent,
  onMounted,
  watch
} from "@vue/composition-api";
import { provideStore } from "@/store/plugin";
import store from "@/store/story";
import { withKnobs, text } from "@storybook/addon-knobs";
import Greeting from "@/widgets/greeting/Greeting.vue";

Vue.use(VueComposition);

export default {
  title: "Greeting",
  decorators: [withKnobs]
};

export const WithKnobs = () =>
  createComponent({
    components: { Greeting },
    template: '<Greeting module="greeting"></Greeting>',
    props: {
      message: {
        default: text("Message", "Welcome to @Home")
      }
    },
    setup(props) {
      provideStore(store);

      watch(
        () => props.message,
        v => store.commit("greeting/updateGreeting", v),
        {
          lazy: true
        }
      );

      onMounted(() => store.commit("greeting/updateGreeting", props.message));

      return {};
    }
  });
