import { Module } from "vuex";
import { WidgetConfig } from "@/store/types";

interface GreetingState {
  greeting: string;
}

export default function(
  name: string,
  settings: () => any
): Module<WidgetConfig & GreetingState, any> {
  return {
    namespaced: true,
    state: {
      name: name,
      description: "Display a greeting in every new tab.",
      src: "Greeting",
      greeting: "Welcome to @Home"
    },
    getters: {
      settings: () => settings
    },
    mutations: {
      updateGreeting(state, greeting: string) {
        state.greeting = greeting;
      },
      changeName(state, name: string) {
        state.name = name;
      }
    }
  };
}
