import { createLocalVue, shallowMount } from "@vue/test-utils";
import Vuex, { Store } from "vuex";
import VueComposition from "@vue/composition-api";
import { StoreSymbol } from "@/store/plugin";
import Greeting from "@/widgets/greeting/Greeting.vue";

const localVue = createLocalVue();

localVue.use(VueComposition);
localVue.use(Vuex);

function wrapGreetings(greeting: any, store: Store<any>) {
  return shallowMount(greeting, {
    propsData: {
      module: "Greeting"
    },
    localVue,
    provide: {
      [StoreSymbol]: store
    }
  });
}

describe("Greeting", () => {
  let store: Store<any>;

  beforeEach(() => {
    store = new Store({
      modules: {
        widgets: {}
      }
    });
  });

  it("should render default greeting", () => {
    const wrapper = wrapGreetings(Greeting, store);

    expect(wrapper.text()).toMatch("Welcome to @Home");
  });

  it("should react to greeting mutation", async () => {
    const wrapper = wrapGreetings(Greeting, store);

    store.commit("Greeting/updateGreeting", "Hello, Test");

    await wrapper.vm.$nextTick();

    expect(wrapper.text()).toMatch("Hello, Test");
  });
});
