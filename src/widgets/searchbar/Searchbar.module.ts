import { Module } from "vuex";
import { WidgetConfig } from "@/store/types";

interface SearchbarState {
  provider: string;
}

export default function(
  name: string,
  settings: () => any
): Module<WidgetConfig & SearchbarState, any> {
  return {
    namespaced: true,
    state: {
      name: name,
      description: "Search for anything you can imagine.",
      src: "Searchbar",
      provider: "Google"
    },
    getters: {
      settings: () => settings
    },
    mutations: {
      changeProvider(state, provider: string) {
        state.provider = provider;
      },
      changeName(state, name: string) {
        state.name = name;
      }
    }
  };
}
