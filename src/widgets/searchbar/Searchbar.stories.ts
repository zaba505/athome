import Vue from "vue";
import VueComposition, { createComponent, watch } from "@vue/composition-api";
import store from "@/store/story";
import { provideStore } from "@/store/plugin";
import { withKnobs, select } from "@storybook/addon-knobs";
import Searchbar from "@/widgets/searchbar/Searchbar.vue";

Vue.use(VueComposition);

export default {
  title: "Searchbar",
  decorators: [withKnobs]
};

export const WithKnobs = () =>
  createComponent({
    components: { Searchbar },
    template: '<Searchbar module="searchbar"></Searchbar>',
    props: {
      provider: {
        default: select(
          "Search Provider",
          [
            "Google",
            "Bing",
            "Baidu 百度",
            "DuckDuckGo",
            "Qwant",
            "Ecosia",
            "Lilo",
            "Startpage",
            "Яндекс",
            "Поиск Mail.Ru"
          ],
          "Google"
        )
      }
    },
    setup(props) {
      provideStore(store);

      watch(
        () => props.provider,
        v => store.commit("searchbar/changeProvider", v),
        {
          lazy: true
        }
      );

      return {};
    }
  });
