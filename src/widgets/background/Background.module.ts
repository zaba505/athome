import { Module } from "vuex";
import { WidgetConfig } from "@/store/types";

type ImageCredit = {
  credits: Array<{ name: string; link: string }>;
  location: string;
};

interface backgroundState {
  provider: string;
}

export default function(
  name: string,
  settings: () => any
): Module<WidgetConfig & backgroundState, any> {
  return {
    namespaced: true,
    state: {
      name: name,
      description: "Customize your background",
      src: "Background",
      provider: ""
    },
    getters: {
      settings: () => settings
    },
    mutations: {
      changeProvider(state, provider: string) {
        state.provider = provider;
      },
      changeName(state, name: string) {
        state.name = name;
      }
    }
  };
}
