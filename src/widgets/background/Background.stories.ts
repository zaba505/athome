import Vue from "vue";
import VueComposition, { createComponent, watch } from "@vue/composition-api";
import store from "@/store/story";
import { provideStore } from "@/store/plugin";
import { color, withKnobs, select } from "@storybook/addon-knobs";
import Background from "@/widgets/background/Background.vue";

Vue.use(VueComposition);

export default {
  title: "Background",
  decorators: [withKnobs]
};

interface UnsplashState {
  provider: "official" | "collections" | "search";
  refresh: number;
  collections: string;
  featured: boolean;
  search: string;
  cache: any[];
}

export const WithKnobs = () =>
  createComponent({
    components: { Background },
    template:
      '<Background module="background" style="width: 256px; height: 256px"></Background>',
    props: {
      provider: {
        default: select(
          "Background",
          ["", "Solid Color", "Unsplash Images"],
          ""
        )
      },
      color: {
        default: color("Color", "#000")
      }
    },
    setup(props) {
      provideStore(store);

      watch(
        () => props.provider,
        v => {
          switch (v) {
            case "Solid Color":
              store.commit("background/changeProvider", "Color");
              break;
            case "Unsplash Images":
              store.commit("background/changeProvider", "Unsplash");
              break;
          }
        },
        {
          lazy: true
        }
      );

      watch(
        () => {
          if (props.provider === "Solid Color") return props.color;
        },
        v => store.commit("background/Color/changeColor", v)
      );

      return {};
    }
  });
