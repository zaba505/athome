import { Module } from "vuex";
import { WidgetConfig } from "@/store/types";

interface ClockState {
  type: number;
  display: string;
  zone: number;
  fontSize: number;
  displayName: boolean;
}

export default function(
  name: string,
  settings: () => any
): Module<WidgetConfig & ClockState, any> {
  return {
    namespaced: true,
    state: {
      name: name,
      description: "Keep track of time.",
      src: "Clock",
      type: 12,
      display: "Digital",
      zone: new Date().getTimezoneOffset() / 60,
      fontSize: 4,
      displayName: false
    },
    getters: {
      settings: () => settings
    },
    mutations: {
      changeType(state, type: number) {
        state.type = type;
      },
      changeDisplay(state, display: string) {
        state.display = display;
      },
      changeZone(state, zone: number) {
        state.zone = zone;
      },
      updateFontSize(state, size: number) {
        state.fontSize = size;
      },
      showDisplayName(state) {
        state.displayName = !state.displayName;
      },
      changeName(state, name: string) {
        state.name = name;
      }
    }
  };
}

// convertHours returns the given local UTC hours in 12 or 24 format and in
// any desired time zone.
//
export const convertHours = (
  hrs: number,
  clockType: 12 | 24,
  clockZone: number
) => ((hrs % clockType) - clockZone + clockType) % clockType;
