import Vue from "vue";
import VueComposition, { createComponent, watch } from "@vue/composition-api";
import store from "@/store/story";
import { provideStore } from "@/store/plugin";
import {
  boolean,
  withKnobs,
  number,
  text,
  optionsKnob as options
} from "@storybook/addon-knobs";
import Clock from "@/widgets/clock/Clock.vue";

Vue.use(VueComposition);

export default {
  title: "Clock",
  decorators: [withKnobs]
};

export const WithKnobs = () =>
  createComponent({
    components: { Clock },
    template:
      '<Clock module="clock" style="width: 256px; height: 256px; margin: auto"></Clock>',
    props: {
      showName: {
        default: boolean("ShowName", false)
      },
      name: {
        default: text("Name", "clock")
      },
      clockType: {
        default: options("Clock Type", { "12": "12", "24": "24" }, "12", {
          display: "inline-radio"
        })
      },
      display: {
        default: options(
          "Display",
          { Analog: "Analog", Digital: "Digital" },
          "Digital",
          {
            display: "inline-radio"
          }
        )
      },
      zone: {
        default: number("Zone", new Date().getTimezoneOffset() / 60, {
          range: true,
          min: -12,
          max: 14,
          step: 1
        })
      },
      fontSize: {
        default: number("Font Size", 4)
      }
    },
    setup(props) {
      provideStore(store);

      watch(
        () => props.clockType,
        v => store.commit("clock/changeType", parseInt(v, 10)),
        {
          lazy: true
        }
      );

      watch(
        () => props.display,
        v => store.commit("clock/changeDisplay", v),
        {
          lazy: true
        }
      );

      watch(
        () => props.zone,
        v => store.commit("clock/changeZone", v),
        {
          lazy: true
        }
      );

      watch(
        () => props.fontSize,
        v => store.commit("clock/updateFontSize", v),
        {
          lazy: true
        }
      );

      watch(
        () => props.showName,
        () => store.commit("clock/showDisplayName"),
        {
          lazy: true
        }
      );

      watch(
        () => props.name,
        v => store.commit("clock/changeName", v),
        {
          lazy: true
        }
      );

      return {};
    }
  });
