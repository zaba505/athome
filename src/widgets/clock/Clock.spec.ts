import { createLocalVue, shallowMount } from "@vue/test-utils";
import VueComposition from "@vue/composition-api";
import Vuex, { Store } from "vuex";
import { StoreSymbol } from "@/store/plugin";
import Clock from "@/widgets/clock/Clock.vue";
import { convertHours } from "@/widgets/clock/Clock.module";

const localVue = createLocalVue();

localVue.use(VueComposition);
localVue.use(Vuex);

function wrapClock(clock: any, store: Store<any>) {
  return shallowMount(clock, {
    propsData: {
      module: "Clock"
    },
    localVue,
    provide: {
      [StoreSymbol]: store
    }
  });
}

describe("Clock", () => {
  let store: Store<any>;

  beforeEach(() => {
    store = new Store({
      modules: {
        widgets: {}
      }
    });
  });

  it("should render digital", async () => {
    const wrapper = wrapClock(Clock, store);

    await wrapper.vm.$nextTick();

    expect(wrapper.text()).toBeTruthy();
  });

  it("should render analog", async () => {
    const wrapper = wrapClock(Clock, store);

    store.commit("Clock/changeDisplay", "Analog");

    await wrapper.vm.$nextTick();

    expect(wrapper.text()).toBeFalsy();

    expect(wrapper.html()).toContain("svg");
  });

  it("should display name", async () => {
    const wrapper = wrapClock(Clock, store);

    store.commit("Clock/showDisplayName");

    await wrapper.vm.$nextTick();

    expect(wrapper.html()).toContain("Clock");
  });
});

describe("convertHours", () => {
  test.each([
    // Test identities
    ...Array.from(new Array(12).keys(), x => [`${x} => 12`, x, 12, 0, x]),
    ...Array.from(new Array(24).keys(), x => [`${x} => 24`, x, 24, 0, x]),

    // Test edges, note: getUTCHours always returns 24 time
    ["24 => 12", 13, 12, 0, 1],
    ["zone != 0", 20, 24, -6, 2] // six hrs ahead cuz browser returns op. sign
  ])("%s", (name, hrs, typ, zone, expected) =>
    expect(convertHours(hrs as number, typ as 12 | 24, zone as number)).toBe(
      expected as number
    )
  );
});
