module.exports = {
  __version: "3.7.0",
  "Update Widget": {
    "should update db on position mutations": {
      "1": "180"
    },
    "should update db on size mutations": {
      "1": "350"
    }
  },
  Draggable: {
    "should drag on mouse down": {
      "1": "180"
    }
  },
  Resizable: {
    "should grow width": {
      "1": "350"
    },
    "should grow height": {
      "1": "40"
    }
  }
};
